from flask import Flask, request, jsonify, send_file
import os
import socket
import cv2
import numpy as np
import torch
from vortex.development.utils.runtime_wrapper import RuntimeWrapper


#Additional Resource: https://stackoverflow.com/questions/28568687/send-with-multiple-csvs-using-flask

app = Flask(__name__)

@app.route("/predict", methods=['POST'])
def predict():

    if('file' not in request.files):
        return jsonify({'msg': 'Check your POST request!'})

    filestr = request.files['file'].read()

    image_name = request.files['file'].filename.split(".")[0]
    # print(image_name)

    img = np.fromstring(filestr, np.uint8)
    img = cv2.imdecode(img, cv2.IMREAD_COLOR)/255

    # img = cv2.imdecode(npimg)/255
    img = cv2.resize(img, (224, 224))

    #Normalize image
    img = (img - 0.5) / 0.5

    #Expand dims so match model input
    img = np.expand_dims(img, 0)

    #Switch channel
    img = np.transpose(img, [0,3,1,2])

    #Convert to torch
    img = torch.from_numpy(img.astype(np.float32))
    
    model_inf = RuntimeWrapper(
        "export_test.onnx",
        runtime='cpu'
    )
    
    pred = model_inf(img)
    labels=["building","forests","mountains","glacier","street","sea"]
    d = {"label": labels[int(pred[0]['class_label'].item())], "confidence": pred[0]['class_confidence'].item()}
    return jsonify(d)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
