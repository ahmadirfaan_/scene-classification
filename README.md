# scene-classification

A flask project with Docker to serve an API that needs an image in a POST request and returns a classification based on a ResNet34 that is trained using the dataset from [Kaggle](https://www.kaggle.com/nitishabharathi/scene-classification) that can classify: 

- Building
- Forest
- Mountain
- Glacier
- Street
- Sea

### HOW TO USE

Clone the project into your local

```
git clone https://gitlab.com/ahmadirfaan_/scene-classification.git
cd scene-classification
```

Build the image (you can change the _scene-classification_ part)
```
docker build -t scene-classification .
```

Run the docker (if you change the image name before, don't forget to change the image name here)
```
docker run -it --rm -p 80:80 scene-classification
```

### HOW TO REQUEST 

You should request a POST into **ipaddress**/predict using

| Parameter |    Type   |
| ------    | ------    |
| file      | jpg/png   |

Input example: 

![test1](/uploads/2058f33f48d064c16420778a85f25f61/test1.jpg)

On Postman: 

![Screenshot_from_2021-09-16_16-39-12](/uploads/13cb2b60f032fe0270b370c487ce9261/Screenshot_from_2021-09-16_16-39-12.png)

JSON Output: 

![Screenshot_from_2021-09-16_16-39-54](/uploads/891bdf5b7b8db52ec230f0f134098be7/Screenshot_from_2021-09-16_16-39-54.png)

### Performance

Confusion Matrix: 

![Screenshot_from_2021-09-16_16-40-44](/uploads/a3f5e190a28ce845676dd9f4f6561a8e/Screenshot_from_2021-09-16_16-40-44.png)

We have an accuracy on validation set on 86.5% using 20 epochs.
